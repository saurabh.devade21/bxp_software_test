const main = require('./main');
// const checkForJohnWickBogeyman=require('./main');
test('10 multiple of 3 == false', () => {
  expect(main.checkIfMultipleOrNot(10, 3)).toBe(false);
});

test('99 multiple of 3 == True', () => {
  expect(main.checkIfMultipleOrNot(99, 3)).toBe(true);
});

test('45 multiple of 5 == True', () => {
  expect(main.checkIfMultipleOrNot(45, 5)).toBe(true);
});
test('15 should be returned as Bogeyman', () => {
  expect(main.checkForJohnWickBogeyman(15)).toBe("Bogeyman");
});
test('27 should be returned as John', () => {
  expect(main.checkForJohnWickBogeyman(27)).toBe("John");
});

test('50 should be returned as Wick', () => {
  expect(main.checkForJohnWickBogeyman(50)).toBe("Wick");
});