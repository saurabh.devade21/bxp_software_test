const Bogeyman = "Bogeyman";
const John = "John";
const Wick = "Wick";

/* This function will return true if
the passed number is multiple of 3 & 5 or not*/
function checkIfMultipleOrNot(integer, divisor) {
  return integer % divisor === 0;
}

/**
 * This function accepts number as a parameter, then this numbers is 
 * checked for is multiplicity with 15,3 & 5 using switch case and based on it the string is returned
 * 
 */
function checkForJohnWickBogeyman(number) {
  switch (true) {
    case checkIfMultipleOrNot(number, 15):
      return Bogeyman;
    case checkIfMultipleOrNot(number, 3):
      return John;
    case checkIfMultipleOrNot(number, 5):
      return Wick;
    default:
      return number;
  }
}

/* 
This function will be invoked by the user click;
Once the function is invoked it will call a checkForJohnWickBogeyman() function for hunderd times &
based on the outpur returned by that function a string of John, Bogeyman & wick will be generated & finally it 
will be showed on the html page

*/
function printJohncWick() {
  let johnWickString = "";
  for (let i = 1; i <= 100; i++) {
    johnWickString += checkForJohnWickBogeyman(i) + "</br>";
  }
  document.getElementById("outputDiv").innerHTML = johnWickString;
}

/**
 * Modeules exported for testing purposes
 */
module.exports = {
  checkIfMultipleOrNot,
  checkForJohnWickBogeyman,
};
