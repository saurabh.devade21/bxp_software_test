
let main=require('./main')

test('missing number should be 4', () => {
    expect(main.findTheMissingNumber([5,6,1,7,3,2],7)).toBe(4);
    
});

test('missing number should not be 5', () => {
    expect(main.findTheMissingNumber([5,6,1,7,3,2],7)).not.toBe(5);
    
});

test('NO MISSING NUMBER SHOULD BE RETURNED', () => {
    expect(main.findTheMissingNumber([5,6,1,7,3,2,4],7)).toBe("NO MISSING NUMBER");
    
});
