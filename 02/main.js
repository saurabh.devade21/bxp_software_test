let shuffledArray = null;
let concatinatedStringOfArrayIntegers = "";

/**
 * This function is responsible for generating the array of n size ,
 * then shuffling it and taking out the number randomly
 * first it creates a array of n numbers using ES6 Array.from
 * 
 * To shuffle the array randomly, a random number is generated and the element at that random position
 * is swapped with the i'th element using for loop. At the end of for loop we get shuffled array.abs
 * 
 * Then finally using splice method a random number is removed from the array
 * 
 */
function generateShuffledArray(n) {
  shuffledArray = Array.from({ length: n }, (_, i) => i + 1);
  for (let i = 0; i < shuffledArray.length; i++) {
    let tempVar = shuffledArray[i];
    let randomNumber = Math.floor(Math.random() * n - 1) + 1;
    shuffledArray[i] = shuffledArray[randomNumber];
    shuffledArray[randomNumber] = tempVar;
  }
  shuffledArray.splice(Math.floor(Math.random() * n - 1) + 1, 1);
  for (let i = 0; i < shuffledArray.length; i++) {
    concatinatedStringOfArrayIntegers += shuffledArray[i] + " ";
  }
}

/**
 * This function finds out the missing number from the shuffled array
 * first it checks if length of original array and spliced array is not same
 * further, it calcultaes the sum of all the elements of original array i.e 1-500
 * once the sum is calculated, the every element of spliced array is subtracted from the total,
 * once the last element is subtracted the remaining total is the missing number 
 * 
 */
function findTheMissingNumber(splicedArray, orginalArrayLength) {
  if (splicedArray.length !== orginalArrayLength) {
    let total = ((splicedArray.length + 1) * (splicedArray.length + 2)) / 2;
    for (let i = 0; i < splicedArray.length; i++) {
      total -= splicedArray[i];
    }

    return total;
  } else {
    return "NO MISSING NUMBER";
  }
}

/**
 * The main function is invoked whn user clicks the button
 * this function includes the sequnetial call for other functions and showing the output on the html page
 * 
 */

function mainFunction() {
  let lengthOfArrayToBegenerated=500;
  shuffledArray = null;
  concatinatedStringOfArrayIntegers = "";
  generateShuffledArray(lengthOfArrayToBegenerated);
  document.getElementById(
    "arrayDiv"
  ).innerHTML = concatinatedStringOfArrayIntegers;
  document.getElementById("missingDiv").innerHTML = findTheMissingNumber(
    shuffledArray,
    lengthOfArrayToBegenerated
  );
}
module.exports = {
  findTheMissingNumber,
};
