let shuffledArray = null;
let concatinatedStringOfArrayIntegers = "";
function generateShuffledArray() {
  shuffledArray = Array.from({ length: 500 }, (_, i) => i + 1);

  for (let i = 0; i < shuffledArray.length; i++) {
    //console.log(Math.floor(Math.random() * 500) + 1 );
    let tempVar = shuffledArray[i];
    let randomNumber = Math.floor(Math.random() * 499) + 1;
    shuffledArray[i] = shuffledArray[randomNumber];
    shuffledArray[randomNumber] = tempVar;
  }
  shuffledArray.splice(Math.floor(Math.random() * 499) + 1, 1);
  for (let i = 0; i < shuffledArray.length; i++) {
    concatinatedStringOfArrayIntegers += shuffledArray[i] + " ";
  }
  document.getElementById(
    "arrayDiv"
  ).innerHTML = concatinatedStringOfArrayIntegers;
}

function findTheMissingNumber(splicedArray) {
  let total = ((splicedArray.length + 1) * (splicedArray.length + 2)) / 2;
  for (let i = 0; i < splicedArray.length; i++) {
    total -= splicedArray[i];
  }
  document.getElementById("missingDiv").innerHTML = total;
  return total;
}
generateShuffledArray();
findTheMissingNumber(shuffledArray);
