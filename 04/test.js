let main = require("./main");

test("The date 7/oct/20 & Time 4:55 PM should return WEDNESDAY", () => {
  expect(main.checkForWednesdayDraw(3, 16)).toBeTruthy();
});

test("The date 7/oct/20 & Time 8:32 PM should return Saturday", () => {
  expect(main.checkForSaturdayDraw(3, 20)).toBeTruthy();
});
