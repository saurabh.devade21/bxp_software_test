const wednesday = 3;
const saturday = 6;
const drawTime = 20;


/**
 * using switch case it check the passed date and time for the valid draws 
 * and returns the output based on it
 *  
 */
function getTheDraw(dayOfWeek, hourOfDay) {
  switch (true) {
    case checkForWednesdayDraw(dayOfWeek, hourOfDay):
      return "WEDNESDAY";
    case checkForSaturdayDraw(dayOfWeek, hourOfDay):
      return "SATURDAY";
    default:
      return "Plase select the valid input";
  }
}


/**
 * Based on the the dayOfWeek & hourOfDay passed to this function will
 * return true if the next draw is on wednesday
 * conditions for wednesday's draw are
 * day should be less than wednesday & greater than saturday OR
 * if day is wednesday the time should be less than the drawtime i.e 8 PM    OR
 * if the day is saturday the time should be grater than drawtime
 * 
 */
function checkForWednesdayDraw(dayOfWeek, hourOfDay) {
  if (
    dayOfWeek < wednesday ||
    dayOfWeek > saturday ||
    (dayOfWeek == wednesday && hourOfDay < drawTime) ||
    (dayOfWeek == saturday && hourOfDay >= drawTime)
  ) {
    return true;
  }
}

/**
 * Based on the the dayOfWeek & hourOfDay passed to this function will
 * return true if the next draw is on saturday
 * conditions for saturday's draw 
 * are day should be greater than wednesday & less than saturday OR
 * if day is saturday the time should be less than the drawtime i.e 8 PM    OR
 * if the day is wednesday the time should be grater than drawtime
 * 
 */
function checkForSaturdayDraw(dayOfWeek, hourOfDay) {
  if (
    (dayOfWeek > wednesday && dayOfWeek < saturday) ||
    (dayOfWeek == saturday && hourOfDay < drawTime) ||
    (dayOfWeek == wednesday && hourOfDay >= drawTime)
  ) {
    return true;
  }
}

/**
 * 
 This function is is invoked either on page load or on user submission
 once the date is recieved it gets the dayofweek i.e 0,1,2,3 etc && hour from the sumbmited dat and time
 and the calls the getDraw method
 */
function readDateTimeValue(date) {
  let d = new Date(date);
  let dayOfWeek = d.getDay();
  let hourOfDay = d.getHours();
  document.getElementById("outputDiv").innerHTML = getTheDraw(
    dayOfWeek,
    hourOfDay
  );
}

module.exports = {
  checkForWednesdayDraw,
  checkForSaturdayDraw,
  getTheDraw,
};
