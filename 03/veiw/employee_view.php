<html>

<head></head>
<title>User List</title>

<body>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Job Title</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($employee_list as $key => $employee) {
                echo '<tr>
                        <td>' . $employee->name . '</td>
                        <td>' . $employee->age . '</td>
                        <td>' . $employee->job_title . '</td>
                     </tr>';
            }
            ?>
        </tbody>
    </table>
</body>

</html>