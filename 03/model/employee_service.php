<?php
include "config.php";
include "employee.php";

/**
 * This class is responsible for all the business logic , calculation and modification neeeded
 * for the employees.
 * It executes queries on the database, made modifications if needed etc.
 * hence named Employee_Service
 */

class Employee_Service
{

    private $db;


    public function __construct()
    {
        $this->db = new config();
    }

    /**
     * This function is responible for fetching all the employees from the database
     * once the connection is made with the databse by calling open_db() method from config class,
     * a preapred statement is created and on executing that we get the results
     * those result are converted to the the employee model object and those object are pushed to the array
     * and thats array of employee object is retuned to the controller method
     *
     * 
     */
    public function getAllEmployees()
    {
        try {
            $this->condb = $this->db->open_db();

            $query = $this->condb->prepare("SELECT * FROM bxp_test");

            $query->execute();
            $res = $query->get_result();
            $query->close();
            $employee_list = [];
            
            while ($row = $res->fetch_assoc()) {
                $employeObj = new Employee();
                $employeObj->name = $row['name'];
                $employeObj->id = $row['id'];
                $employeObj->age = $row['age'];
                $employeObj->job_title = $row['job_title'];
                array_push($employee_list, $employeObj);
            }
            
            $this->condb->close();
            return $employee_list;
        } catch (Exception $e) {
            $this->condb->close();
            throw $e;
        }
    }

    /**
     * This method takes care of adding the new employees to the database
     * once the query is executed successfully a page is refreshed so the new data will be fetched fro server
     */
    public function addNewEmployee($name, $age, $job_title)
    {
        try {
            $this->db = $this->db->open_db();
            $query = $this->db->prepare("INSERT INTO bxp_test (name, age, job_title) VALUES (?, ?, ?)");
            $query->bind_param("sis", $name, $age, $job_title);
            $query->execute();
            $query->close();
            $this->db->close();
            header("Refresh:0");
        } catch (Exception $e) {
            $this->db->close();
            throw $e;
        }
    }

   
}
