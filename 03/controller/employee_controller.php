<?php
include "./model/employee_service.php";

/**
 * This class takes control of the application
 * It takes care of which method to call from service layer in order to achieve the desired output
 * It controls how user interact with the application
 */

class Employee_Controller
{
    private $employee_service;

    public function __construct()
    {
        $this->employee_service = new Employee_Service();
    }

    public function getAllEmployees()
    {

        /**calls method from Employee_service class to get all employees */
        $employee_list = $this->employee_service->getAllEmployees();
        include "./veiw/employee_view.php";
    }

    public function addNewEmployee($name, $age, $job_title)
    {
        /**calls method from Employee_service class to add new employee */
        $this->employee_service->addNewEmployee($name, $age, $job_title);
    }
}
