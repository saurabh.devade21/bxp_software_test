
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    NAME : <input type="text" name="name">

    AGE : <input type="number" name="age">
    JOB TITLE : <input type="text" name="job_title">
    <input type="submit" value="click" name="submit"> <!-- assign a name for the button -->
</form>
<?php

/**
 * once user submits the form all the input fileds are vallidated & sanitized against various parameters,
 * after succesfully validating all the input fields it calls the addNewEmployee Method from the service layer
 * 
 */
if (isset($_POST['submit'])) {
    $name = validateName($_POST['name']);
    $age = validateAge($_POST['age']);
    $job = validateJobTitle($_POST['job_title']);
    if ($name != false && $age != false && $job != false) {
        $ec->addNewEmployee($name, $age, $job);
    }
}

/**
 * This function validate the name entered by user to check
 * if its not empty and contains only white spaces and characters and the length of name should not exceed by 50
 * else respective error will be thrown
 * 
 */
function validateName($name)
{
    $name = sanitizeInputStrings($name);
    if (empty($name)) {
        $nameErr = "Name is required</br>";
        echo $nameErr;
        return false;
    } elseif (!preg_match("/^[a-zA-Z ]*$/", $name)) {
        $nameErr = "Only alphabets and white space are allowed</br>";
        echo $nameErr;
        return false;
    } elseif (strlen($name) > 50) {
        $nameErr = "The Length of name cannot be greater than 50 chars</br>";
        echo $nameErr;
        return false;
    } else {
        return $name;
    }
}


/**
 * This function validate the job title entered by user to check
 * if its not empty and contains only white spaces, characters & numbers and the length of job title should not exceed by 50
 * else respective error will be thrown
 * 
 */
function validateJobTitle($job)
{
    $job = sanitizeInputStrings($job);
    if (empty($job)) {
        $jobErr = "Job Title is required </br>";
        echo $jobErr;
        return false;
    } elseif (!preg_match("/^[a-zA-Z0-9 ]*$/", $job)) {
        $jobErr = "Only alphabets and white space & numbers are allowed</br>";
        echo $jobErr;
        return false;
    } elseif (strlen($job) > 50) {
        $jobErr = "The Length of Job Title cannot be greater than 50 chars</br>";
        echo $jobErr;
        return false;
    } else {
        return $job;
    }
}


/**
 * This function validate the age entered by user to check
 * if its not empty and contains only numbers and age should not exceed by 100
 * else respective error will be thrown
 * 
 */
function validateAge($age)
{
    if (empty($age)) {
        $ageErr = "Age is required</br>";
        echo $ageErr;
        return false;
    } elseif (!preg_match("/^[0-9]*$/", $age)) {
        $ageErr = "Please enter the valid age</br>";
        echo $ageErr;
        return false;
    } elseif ($age > 100) {
        $ageErr = "Please enter the valid age</br>";
        echo $ageErr;
        return false;
    } else {
        return $age;
    }
}

/**
 * For sanitizing the various inputs various FIlters are used
 */
function sanitizeInputStrings($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $data = filter_var((filter_var(filter_var($data, FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRIPPED)), FILTER_SANITIZE_SPECIAL_CHARS);
    return $data;
}

?>