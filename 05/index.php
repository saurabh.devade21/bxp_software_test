<?php

include "./controller/design_controller.php";
$dc = new Design_Controller();

//echo +"</br>"+$_GET['emp'];


/** this logic fethes employee id from the url and pass it to the redirectToDesign function
 * in controller
 */
if (isset($_GET['emp']) && is_numeric($_GET['emp'])) {
    $dc->redirectToDesign($_GET['emp']);
} else {
    echo "PLEASE ENTER THE EMPLOYEE ID TO REDIRECT";
}