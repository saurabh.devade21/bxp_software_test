<?php
include "./model/design_service.php";


/**
 * This class takes control of the application
 * It takes care of which method to call from service layer in order to achieve the desired output
 * It controls how user interact with the application
 */
class Design_Controller
{
    private $design_service;


    public function __construct()
    {
        $this->design_service = new Design_Service();
    }

    /**
     * Once this function is invoked from the index page
     * the respective functions are called sequentially explained in Design_Service
     */
    public function redirectToDesign($reeuestedEmployee)
    {
        $designs = $this->design_service->getDesigns();
        $employees=$this->design_service->getAllEmployees();
        $employeePosition=$this->design_service->getPositionOfEmployee($employees,$reeuestedEmployee);
        $slots=$this->design_service->makeSlots($designs,sizeof($employees));
        $this->design_service->redirection($employeePosition,$slots);
    }

}
