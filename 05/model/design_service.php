<?php
include "config.php";
include "employee.php";
include "design.php";
include "slots.php";
class Design_Service
{
    private $db;
    public function __construct()
    {
        $this->db = new config();
    }

    /**
     * After creating a connection with database this function fetches all the availabale design
     * from the table and fits it into the model class of design
     */
    public function getDesigns()
    {
        try {
            $this->condb = $this->db->open_db();
            $query = $this->condb->prepare("SELECT * FROM bxp_designs");
            $query->execute();
            $res = $query->get_result();
            $query->close();
            $designs = [];
            while ($row = $res->fetch_assoc()) {
                $design = new Design();
                $design->design_id = $row['design_id'];
                $design->design_name = $row['design_name'];
                $design->split_percent = $row['split_percent'];
                array_push($designs, $design);
            }
            $this->condb->close();
            return $designs;
        } catch (Exception $e) {
            // $this->db->close_db();
            throw $e;
        }
    }

    /**
     * After creating a connection with database this function fetches all the employees
     * from the table and fits it into the model class of Employee
     */
    public function getAllEmployees()
    {
        try {
            $this->condb = $this->db->open_db();

            $query = $this->condb->prepare("SELECT * FROM bxp_test");

            $query->execute();
            $res = $query->get_result();
            $query->close();
            $employee_list = [];
            while ($row = $res->fetch_assoc()) {
                $employeObj = new Employee();
                $employeObj->name = $row['name'];
                $employeObj->id = $row['id'];
                $employeObj->age = $row['age'];
                $employeObj->job_title = $row['job_title'];
                array_push($employee_list, $employeObj);
            }
            $this->condb->close();
            return $employee_list;
        } catch (Exception $e) {
            // $this->db->close_db();
            throw $e;
        }
    }

    /***
     * This function is written for making the slots as per the percentages of designs
     * it first calclulate the percentages from the avialaible employees
     * for eg - if employees are 8 and design A has 50% then 4 employees will see design A & so on
     * after that to makes slots previous percentages are added to the current to make the range
     * fro eg - 
    *       design A 50% = 4 Employees, 
            design B 25% = 2 Employees, 
            Design c 25% = 2 employees
     * then slots would be 
     * design A 1 to 4 
     * design B 5 to 6
     * design C 7 to 8
     * 
     * after that slots are converted to to model object of slots and pushed to the array
     */
    public function makeSlots($designs,$employeeCount)
    {
        $slots = [];
        $percent = 0;
        for ($i = 0; $i < sizeof($designs); $i++) {
            $slotObj = new Slots();
            $percent = ceil($percent + ($designs[$i]->split_percent / 100) * $employeeCount);
            $slotObj->slot = $percent;
            $slotObj->design_name = $designs[$i]->design_name;
            array_push($slots, $slotObj);
        }
        return $slots;
    }

    /**
     * this functions perfomrs the operation of redirecting users to specific design based on percentages
     * once it gets the $employeePostion i.e postion of employee in the database and available slots
     * it check if employees postion comes in that slot if it comes then it redirects to that design
     * if slots are like 
     * design A 1 to 4 
     * design B 5 to 6
     * design C 7 to 8
     * 
     * and employee postion in the database is 3
     * it checks if employee postion is greater than slot 1 - 4 , if not then this is the slot for that employee
     */
    public function redirection($employeePosition, $slots)
    {
        for ($i = 0; $i < sizeof($slots); $i++) {
            if (!($employeePosition >= $slots[$i]->slot)) {
                echo $slots[$i]->design_name;
                break;
            }
        }
    }

    /**
     * This function will return the postion of employee in the database .i.e row number of the employee
     */
    public function getPositionOfEmployee($employees, $requestedEmployee)
    {
        $employeesIdList = [];
        for ($i = 0; $i < sizeof($employees); $i++) {
            
            array_push($employeesIdList,$employees[$i]->id);
        }
        return array_search($requestedEmployee, $employeesIdList);
    }
}
