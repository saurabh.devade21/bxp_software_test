<?php  
class config    
{   
    private $condb;
    
    
    function __construct() {  
        $this->host = "localhost";  
        $this->user  = "BXP_At_Home_Name";  
        $this->pass = "BXP_At_Home_Key";  
        $this->db = "bxp_at_home";  
        
    }
    public function open_db()  
    {  
        $this->condb=new mysqli($this->host,$this->user,$this->pass,$this->db);  
        if ($this->condb->connect_error)   
        {  
            die("Erron in connection: " . $this->condb->connect_error);  
        }
        return $this->condb;  
    }
}  
?>  